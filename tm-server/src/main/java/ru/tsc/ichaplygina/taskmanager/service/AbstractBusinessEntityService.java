package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessEntityService<E> {

    @NotNull
    protected final IUserService userService;

    public AbstractBusinessEntityService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService);
        this.userService = userService;
    }

    @NotNull
    @Override
    protected abstract IBusinessEntityRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String entityName, @Nullable final String entityDescription) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            repository.add(userId, entityName, entityDescription);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            if (userService.isPrivilegedUser(userId)) repository.clear();
            else repository.clearForUser(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E completeById(@NotNull final String userId, @NotNull final String entityId) {
        return updateStatusById(userId, entityId, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByIndex(@NotNull final String userId, final int entityIndex) {
        return updateStatusByIndex(userId, entityIndex, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByName(@NotNull final String userId, @NotNull final String entityName) {
        return updateStatusByName(userId, entityName, Status.COMPLETED);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll() : repository.findAllForUser(userId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        final Comparator<E> comparator = getComparator(sortBy);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll(comparator) : repository.findAllForUser(userId, comparator);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String entityId) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findById(entityId) : repository.findByIdForUser(userId, entityId);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int entityIndex) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getId(entityIndex) : repository.getIdForUser(userId, entityIndex);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getId(entityName) : repository.getIdForUser(userId, entityName);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getSize() : repository.getSizeForUser(userId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.isEmpty() : repository.isEmptyForUser(userId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isNotFoundById(@NotNull final String userId, @NotNull final String entityId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.isNotFoundById(entityId) : repository.isNotFoundByIdForUser(userId, entityId);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String entityId) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.removeById(entityId) : repository.removeByIdForUser(userId, entityId);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.removeByIndex(entityIndex) : repository.removeByIndexForUser(userId, entityIndex);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.removeByName(entityName) : repository.removeByNameForUser(userId, entityName);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E startById(@NotNull final String userId, @NotNull final String entityId) {
        return updateStatusById(userId, entityId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByIndex(@NotNull final String userId, final int entityIndex) {
        return updateStatusByIndex(userId, entityIndex, Status.IN_PROGRESS);
    }

    @Nullable
    @Override

    public E startByName(@NotNull final String userId, @NotNull final String entityName) {
        return updateStatusByName(userId, entityName, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E updateById(@NotNull final String userId,
                        @NotNull final String entityId,
                        @NotNull final String entityName,
                        @Nullable final String entityDescription) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.update(entityId, entityName, entityDescription) :
                    repository.updateForUser(userId, entityId, entityName, entityDescription);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E updateByIndex(@NotNull final String userId,
                           final int entityIndex,
                           @NotNull final String entityName,
                           @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E updateStatusById(@NotNull final String userId,
                              @NotNull final String entityId,
                              @NotNull final Status status) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.updateStatusById(entityId, status) :
                    repository.updateStatusByIdForUser(userId, entityId, status);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E updateStatusByIndex(@NotNull final String userId,
                                 final int projectIndex,
                                 @NotNull final Status status) {
        if (isInvalidListIndex(projectIndex, getSize(userId))) throw new IndexIncorrectException(projectIndex + 1);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.updateStatusByIndex(projectIndex, status) :
                    repository.updateStatusByIndexForUser(userId, projectIndex, status);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E updateStatusByName(@NotNull final String userId,
                                @NotNull final String projectName,
                                @NotNull final Status status) {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IBusinessEntityRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = userService.isPrivilegedUser(userId) ?
                    repository.updateStatusByName(projectName, status) :
                    repository.updateStatusByNameForUser(userId, projectName, status);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
