package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.sql.Connection;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void add(@NotNull final E entity) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.add(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<E> entities) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.addAll(entities);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findAll();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findById(id);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.getSize();
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.isEmpty();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            @Nullable final E entity = repository.removeById(id);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
