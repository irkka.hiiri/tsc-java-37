package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;

import java.sql.Connection;

public final class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService, userService);
    }

    @Override
    @NotNull
    protected IBusinessEntityRepository<Project> getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }
}
