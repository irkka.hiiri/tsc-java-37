package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_TASK";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public @NotNull Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setStatus(Status.getStatus(row.getString("STATUS")));
        task.setCreated(row.getTimestamp("CREATED_DT"));
        task.setDateStart(row.getTimestamp("STARTED_DT"));
        task.setDateFinish(row.getTimestamp("COMPLETED_DT"));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final Task task) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(task.getCreated().getTime()));
        statement.setTimestamp(6, task.getDateStart() == null ? null : new Timestamp(task.getDateStart().getTime()));
        statement.setTimestamp(7, task.getDateFinish() == null ? null : new Timestamp(task.getDateFinish().getTime()));
        statement.setString(8, task.getUserId());
        statement.setString(9, task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Task task = new Task(name, description, userId);
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, NAME, DESCRIPTION, STATUS, CREATED_DT, USER_ID) VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, name);
        statement.setString(3, description);
        statement.setString(4, task.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(task.getCreated().getTime()));
        statement.setString(6, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task addTaskToProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (isNotFoundById(taskId)) return null;
        @NotNull final String sql = "UPDATE " + getTableName() + " SET PROJECT_ID = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, taskId);
        statement.executeUpdate();
        statement.close();
        return findById(taskId);
    }

    @Nullable
    @Override
    public final Task addTaskToProjectForUser(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (isNotFoundByIdForUser(userId, taskId)) return null;
        return addTaskToProject(taskId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectId(@NotNull final String projectId) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE PROJECT_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        return findAllByProjectId(projectId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectIdForUser(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? AND PROJECT_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectIdForUser(@NotNull final String userId, @NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        return findAllByProjectIdForUser(userId, projectId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public final boolean isNotFoundTaskInProject(@NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE ID = ? AND PROJECT_ID = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, taskId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean result = !resultSet.next();
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE PROJECT_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task removeTaskFromProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (isNotFoundById(taskId)) return null;
        if (isNotFoundTaskInProject(taskId, projectId)) return null;
        @NotNull final String sql = "UPDATE " + getTableName() + " SET PROJECT_ID = null WHERE ID = ? AND PROJECT_ID= ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, taskId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
        return findById(taskId);
    }

    @Nullable
    @Override
    public final Task removeTaskFromProjectForUser(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (isNotFoundByIdForUser(userId, taskId)) return null;
        return removeTaskFromProject(taskId, projectId);
    }

}
