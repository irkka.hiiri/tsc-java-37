package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractModelRepository<E extends AbstractModel> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractModelRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @Override
    public abstract void add(@NotNull final E entity);

    @NotNull
    public abstract E fetch(@NotNull final ResultSet row);

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) return;
        for (final E entity : entities) add(entity);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = "DELETE FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE ID = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = "SELECT COUNT(1) FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        resultSet.next();
        final int size = resultSet.getInt(1);
        statement.close();
        return size;
    }

    @Override
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public void remove(final @NotNull E entity) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
