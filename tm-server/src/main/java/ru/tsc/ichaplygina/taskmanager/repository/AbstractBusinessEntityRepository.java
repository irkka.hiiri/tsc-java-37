package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractModelRepository<E> implements IBusinessEntityRepository<E> {

    public AbstractBusinessEntityRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final E entity) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(entity.getCreated().getTime()));
        statement.setTimestamp(6, entity.getDateStart() == null ? null : new Timestamp(entity.getDateStart().getTime()));
        statement.setTimestamp(7, entity.getDateFinish() == null ? null : new Timestamp(entity.getDateFinish().getTime()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clearForUser(@NotNull final String userId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAllForUser(@NotNull final String userId) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAllForUser(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return findAllForUser(userId).stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByIdForUser(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE ID = ? AND USER_ID = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByIndex(final int index) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByIndexForUser(@NotNull final String userId, final int index) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByName(@NotNull final String name) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE NAME = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByNameForUser(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? AND NAME = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    public List<String> findKeysForUser(@NotNull final String userId) {
        return findAllForUser(userId).stream()
                .filter(predicateByUser(userId))
                .map(E::getId)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String getId(@NotNull final String name) {
        @Nullable final E entity = findByName(name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getId(final int index) {
        @Nullable final E entity = findByIndex(index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdForUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findByIndexForUser(userId, index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdForUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findByNameForUser(userId, name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Override
    @SneakyThrows
    public int getSizeForUser(@NotNull final String userId) {
        @NotNull final String sql = "SELECT COUNT(1) FROM " + getTableName() + " WHERE USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final int size = resultSet.getInt(1);
        statement.close();
        return size;
    }

    @Override
    public boolean isEmptyForUser(@NotNull final String userId) {
        return findAllForUser(userId).isEmpty();
    }

    @Override
    public boolean isNotFoundById(@NotNull final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findByIdForUser(userId, id) == null;
    }

    @NotNull
    public Predicate<AbstractBusinessEntity> predicateByUser(@NotNull final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Nullable
    @Override
    public E removeByIdForUser(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findByIdForUser(userId, id);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(final int index) {
        @Nullable final E entity = findByIndex(index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndexForUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findByIndexForUser(userId, index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String name) {
        @Nullable final E entity = findByName(name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByNameForUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findByNameForUser(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E update(@NotNull final String id, @NotNull final String name, @Nullable final String description) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET NAME = ?, DESCRIPTION = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
        return findById(id);
    }

    @Nullable
    @Override
    public E updateForUser(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @Nullable final String description) {
        if (isNotFoundByIdForUser(userId, id)) return null;
        return update(id, name, description);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E updateStatusById(@NotNull final String id, @NotNull final Status status) {
        @NotNull final String sql;
        if (status.equals(Status.IN_PROGRESS))
            sql = "UPDATE " + getTableName() + " SET STATUS = ?, STARTED_DT = ? WHERE ID = ?";
        else sql = "UPDATE " + getTableName() + " SET STATUS = ?, COMPLETED_DT = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, status.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
        return findById(id);
    }

    @Nullable
    @Override
    public E updateStatusByIdForUser(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (isNotFoundByIdForUser(userId, id)) return null;
        return updateStatusById(id, status);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(final int index, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(index));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIndexForUser(@NotNull final String userId, final int index, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, index));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String name, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(name));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByNameForUser(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, name));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

}
