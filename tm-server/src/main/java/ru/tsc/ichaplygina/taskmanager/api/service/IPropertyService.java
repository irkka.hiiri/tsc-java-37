package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.property.*;

public interface IPropertyService extends IPasswordProperty, IApplicationProperty, IAutosaveProperty,
        IFileScannerProperty, ISessionProperty, INetworkProperty, IDatabaseProperty {

}
